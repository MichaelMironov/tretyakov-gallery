package ru.tretyakovgallery;

import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите количество чисел последовательности: ");
        int n = scanner.nextInt();

        System.out.printf("Последовательность чисел: \n%s",fibonacci(n));

    }

    static StringBuilder fibonacci(int n){

        int a = 1, b = 1;
        String result = a+" "+b;
        StringBuilder stringBuilder = new StringBuilder(result);
        for (int i = 3; i<= n; i++){
            b = a + b;
            a = b - a;
            stringBuilder.append(" ").append(b);
        }

        return stringBuilder;
    }
}
