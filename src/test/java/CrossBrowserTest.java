import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class CrossBrowserTest {

    private RemoteWebDriver driver;
    private static String URL;
    private static String username;
    private static String auth_key;
    boolean status = false;

    @BeforeClass
    public static void setup() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("user.properties"));
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("config.properties"));
        URL = System.getProperty("url");
        username = System.getProperty("username");
        auth_key = System.getProperty("auth_key");

    }

    @Test(dataProvider= "Set_Environment", description = "Site Launch Check")
    public void homePageDisplayTest(Platform platform, String browserName, String browserVersion)
    {
        DesiredCapabilities capability = new DesiredCapabilities();

        capability.setPlatform(platform);
        capability.setBrowserName(browserName);
        capability.setVersion(browserVersion);

        capability.setCapability("build", "Java CrossBrowser_testing");
        capability.setCapability("name", "Java CrossBrowser_testing");
        capability.setCapability("network", true);//to enable network logs
        capability.setCapability("visual", true);//to enable screenshots
        capability.setCapability("video", false);//to enable video
        capability.setCapability("console", true);//to enable console logs

        try
        {
            driver = new RemoteWebDriver(new URL("https://" + username+ ":" + auth_key + URL), capability);
        }
        catch (Exception e)
        {
            System.out.println("Invalid grid URL" + e.getMessage());
        }

        try
        {
            driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
            driver.get("https://www.tretyakovgallery.ru/");

            WebElement buyBtn = driver.findElement(By.xpath("//a[@href=\"/tickets/afisha/\"]"));

            Assert.assertEquals(driver.getCurrentUrl(),"https://www.tretyakovgallery.ru/?lang=ru");
            Assert.assertTrue(buyBtn.isDisplayed());
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    @DataProvider(name="Set_Environment", parallel=true)
    public static Object[][] getData()
    {
        return new Object[][]
                {
                        {Platform.WIN10, "chrome", "103.0.5060.114"},
                        {Platform.WIN10, "firefox", "101.01"}
                };
    }

    @AfterClass
    public void tearDown()
    {
        if (driver != null) {
            ((JavascriptExecutor) driver).executeScript("lambda-status=" + status);
            driver.quit();
        }
    }
}